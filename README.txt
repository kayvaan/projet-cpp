				CREATION D’UNE APPLICATION
			      DE GESTION DE COMPTES CLIENTS

Au travers de ce projet, vous reverrez beaucoup de points abordés dans l’ensemble des modules. Il s’agit de
gérer des Clients dotés de comptes bancaires.
Le projet sera décomposé en plusieurs parties.
- Partie 1 : Gestion des Clients
- Partie 2 : Base de données des Comptes Bancaires
- Partie 3 : Traitement des Mouvements Bancaires
- Partie 4 : Contraintes techniques
- Partie 5 : Annexes


PARTIE 1 : GESTION DES CLIENTS

On distingue différents types de clients :
- Des Clients « Particuliers »
- Des Clients « Professionnels »
Pour l’ensemble des clients, on distingue les caractéristiques suivantes :
- Identifiant (Entier)
- Nom (Chaine de 50 car. Max)
- Adresse Postale (Libellé, Complément, Code Postal, Ville)
- Mail de contact (doit contenir un @)
Pour les Clients Particuliers, on stockera les informations suivantes :
- Date de Naissance
- Prénom (Chaine de 50 car. Max)
- Sexe (F/M)
Pour les Clients Professionnels, on stockera les informations suivantes :
- Siret (14 chiffres)
- Statut Juridique (SARL, SA, SAS, EURL)
- Adresse du Siège (Libellé, Complément, Code Postal, Ville)
Il est évident que vous vérifierez la véracité de chaque information fournie et vous prévoirez les messages
adéquats en cas de valeurs fournies erronées.
Vous utiliserez le jeu de valeurs suivant afin de constituer une série de données qui coïncidera avec les parties
suivantes.
Quelques conseils :
- Schématiser votre hiérarchie au sein d’un diagramme de classes afin d’avoir une vue d’ensemble de
l’ensemble des classes créées
- Créer dans un premier temps une application console vous permettant de tester les classes mises en
place.


PARTIE 2 : BASE DE DONNEES DES COMPTES BANCAIRES

Par ailleurs, l’ensemble des comptes bancaires est recensé au sein d’une base de données « BdComptes.db ».

Cette dernière contient une table « comptes » dans laquelle on retrouve les informations suivantes :

- Numéro de Compte
- Date Ouverture du compte
- Solde
- Montant du découvert autorisé
- Identifiant Client


PARTIE 3 : TRAITEMENT DES MOUVEMENTS BANCAIRES

De plus, la Banque reçoit périodiquement un fichier nommé « operations.txt » récapitulant l’ensemble des
opérations bancaires réalisées. Ce fichier figure dans les sources fournies.

Les opérations concernées au sein de ce fichier sont:

• Les retraits effectués sur le distributeur automatique de billets
• Les factures carte bleue
• Les dépôts guichet

Chaque opération est identifiée par un code opération selon la correspondance suivante :

1 : Retrait DAB
2 : Paiement Carte Bleue
3 : Dépôt Guichet

Les opérations d’un type différent doivent être enregistrées dans un fichier « Anomalies.log ».


PARTIE 4 : CONTRAINTES TECHNIQUES

Vous constituerez une application graphique qui offrira à l’utilisateur l’ensemble des fonctionnalités
précédemment présentées.

On retrouvera principalement les fonctionnalités suivantes :

- Listing des clients
- Consultation des comptes
- Importation des opérations bancaires

Constituer une interface graphique simple et conviviale.

D’un point de vue technique, on devra retrouver, au sein de votre code, les concepts suivants :

- Héritage
- Surcharge d’Opérateurs
- Composition/Agrégation
- STL
- Exception

Votre projet devra être disponible sur GitLab. Il devra comporter au moins un commit pour chaque
fonctionnalité ajoutée et en état de fonctionnement. Vous pouvez effectuer évidemment des COMMIT
intermédiaires.

- Les différents commit doivent bien être détaillés.
- Vous créerez au moins une branche pour gérer la fonctionnalité « Importer le fichier des Opérations
Bancaires ».

Vous définirez également des Tags pour chaque version stable.

- Commit 1 : Ajout Fichier README.txt comportant l’énoncé
- Commit N : …

Parmi les fichiers sources, vous trouverez 
:
- Donnees Projets.xlsx : Fichier contenant les données présentées dans ce document
- BdComptes.db : Base de données des Comptes Bancaires
- Operations.txt : Fichier contenant les mouvements bancaires à traiter
------------------

A la fin du projet, vous m’enverrez un mail à l’adresse suivante : steeve@ajc-formation.fr comportant :
- En Objet : VIRTUALROOM – Projet CPP – JJ/MM/AAAA – Nom Prénom
- En Corps : le lien vers votre Gitlab accessible en lecture.