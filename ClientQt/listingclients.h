#ifndef LISTINGCLIENTS_H
#define LISTINGCLIENTS_H

#include <QDialog>
#include <QListWidget>
#include "initmap.h"

namespace Ui {
class ListingClients;
}

class ListingClients : public QDialog
{
    Q_OBJECT

public:
    explicit ListingClients(QWidget *parent = nullptr);
    ~ListingClients();

private slots:
    void on_btnQuitter_clicked();

    void on_lstClients_itemDoubleClicked(QListWidgetItem *item);

    void closeEvent(QCloseEvent *event);

private:
    Ui::ListingClients *ui;
};

#endif // LISTINGCLIENTS_H
