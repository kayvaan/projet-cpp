#ifndef ADRESSE_H
#define ADRESSE_H

#include <QString>
#include <QtDebug>

class Adresse
{
public:
    Adresse(QString,QString,QString,QString);
    virtual ~Adresse();

    virtual QString getLibelle() const;
    void setLibelle(const QString &value);

    virtual QString getComplement() const;
    void setComplement(const QString &value);

    virtual QString getCodePostale() const;
    void setCodePostale(QString value);

    virtual QString getVille() const;
    void setVille(const QString &value);

private:
    QString libelle;
    QString complement;
    QString CodePostale;
    QString ville;
};

#endif // ADRESSE_H
