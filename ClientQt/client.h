#ifndef CLIENT_H
#define CLIENT_H

#include <QMainWindow>
#include <QWidget>
#include <QString>
#include "argexceptions.h"
#include "adresse.h"

using namespace std;

class Client
{
public:
    Client(int,QString,QString,QString,QString,QString,QString);
    virtual ~Client();

    int getIdentifiant() const;
    void setIdentifiant(int value);

    QString getNom() const;
    void setNom(const QString &value);

    QString getMail() const;
    void setMail(const QString &value);

    void afficherClient();

    QString GetIdentQStr();


    Adresse *adr=nullptr;
    Adresse *getAdr() const;

private:
    int Identifiant;
    QString Nom;
    QString mail;

};

#endif // CLIENT_H
