#ifndef MODELMAP_H
#define MODELMAP_H

#include <QObject>
#include <QAbstractTableModel>
#include <QMap>
#include "client.h"

class ModelTab : public QAbstractTableModel
{
    Q_OBJECT
public:

    enum RolesMap
    {
        KeyRole = Qt::UserRole + 1,
        ValueRole
    };

    explicit ModelTab(QObject *parent = nullptr);
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    inline void setMap(QMap<int, Client*>* map) { _map = map; }

private:
    QMap<int, Client*>* _map;
};

#endif // MODELMAP_H
