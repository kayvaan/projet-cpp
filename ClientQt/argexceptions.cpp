#include "argexceptions.h"


ArgExceptions::ArgExceptions(Erreurs)
{
    qDebug() << "Destruction de ArgException !!!";
}

ArgExceptions::~ArgExceptions()
{
    qDebug() << "Destruction !!!";
}

QString ArgExceptions::Getmessage() const
{
    QString msg;
    switch (this->GetcodeErr())
    {
    case Erreurs::ERR_LG:
        this->message = "Depassement Longueur Maximum. Max 50 car.";
        break;
    case Erreurs::ERR_MAIL:
        this->message = "Adresse mail inscrite incorrecte (@ absent) !!!";
        break;
    case Erreurs::ERR_SIRET:
        this->message = "Erreur SIRET !!! Doit contenir 14 chiffres !!!";
        break;
    case Erreurs::ERR_STATUT:
        this->message = "Erreur Statut Juridique !!! Uniquement SARL, SA, SAS, EURL sont acceptées !!!";
        break;
    case Erreurs::ERR_CP:
        this->message = "Erreur Code Postal !!! Doit être composé de 5 chiffres !!!";
        break;
    default:
        this->message = "Cas non gere !";
        break;
    }
    msg = (this->message);
    return msg;
}

const char *ArgExceptions::what()
{
    return Getmessage().toStdString().c_str();
}
