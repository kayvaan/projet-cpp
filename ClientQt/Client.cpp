#include "client.h"


Client::Client(int ID, QString n, QString lib, QString comp, QString CP, QString ville, QString mail)
{
    this->setIdentifiant(ID);
    this->setNom(n);
    this->setMail(mail);
    this->adr = new Adresse(lib,comp,CP,ville);
}

Client::~Client()
{
    qDebug() << "Destruction du client : " << getIdentifiant();
}

int Client::getIdentifiant() const
{
    return Identifiant;
}

void Client::setIdentifiant(int value)
{
    Identifiant = value;
}

QString Client::getNom() const
{
    return Nom;
}

void Client::setNom(const QString &value)
{
    Nom = value;
}

QString Client::getMail() const
{
    return mail;
}

void Client::setMail(const QString &value)
{
    mail = value;
}

void Client::afficherClient()
{
    QString details = "Client ID : " + (QString::number(getIdentifiant()))
            + ", nom : " + getNom() + " habitant à l'adresse : " + adr->getLibelle() + " "
            + adr->getComplement() + " " + adr->getVille() + " " + adr->getCodePostale() +
            " Adresse mail associee : " + getMail();
    qDebug() << details;
}

QString Client::GetIdentQStr()
{
    QString id = QString::number(Identifiant);
    id = id.rightJustified(5,'0');
    return id;
}

Adresse* Client::getAdr() const
{
    return adr;
}


