#include "initmap.h"
#include "particuliers.h"
#include "professionnel.h"
#include <QDebug>

QMap<int, Client *> InitMap::mapClients;

void InitMap::alimenter()
{
    Particuliers *p1 = new Particuliers(1,"BETY","12,rue des Oliviers","","94000","CRETEIL","bety@gmail.com","Daniel","Masculin",QDate(1985,11,12));
    Professionnel *pro1 = new Professionnel(2,"AXA","125,rue Lafayette","Digicode 1432","94120","FONTENAY SOUS BOIS","info@axa.fr","12548795641122","SARL","125,rue Lafayette","Digicode 1432","94120","FONTENAY SOUS BOIS");
    Particuliers *p2 = new Particuliers(3,"BODIN","10,rue des Oliviers","Etage 2","94300","VINCENNES","bodin@gmail.com","Justin","Masculin",QDate(1965,5,5));
    Professionnel *pro2 = new Professionnel(4,"PAUL","36, quai des Orfèvres","","93500","ROISSY EN FRANCE","info@paul.fr","87459564455444","EURL","10, esplanade de la Défense","","92060","LA DEFENSE");
    Particuliers *p3 = new Particuliers(5,"BERRIS","15, rue de la République","","94120","FONTENAY SOUS BOIS","berris@gmail.com","Karine","Féminin",QDate(1977,06,06));
    Professionnel *pro3 = new Professionnel(6,"PRIMARK","32, rue E. Renan","Bat. C","75002","PARIS","contact@•primark.fr","08755897458455","SARL","32, rue E. Renan","Bat. C","75002","PARIS");
    Particuliers *p4 = new Particuliers(7,"ABENIR","25, rue de la Paix","","92100","LA DEFENSE","abenir@gmail.com","Alexandra","Féminin",QDate(1977,04,12));
    Professionnel *pro4 = new Professionnel(8,"ZARA","23 av P. Valery","","92100","LA DEFENSE","info@zara.fr","65895874587854","SA","24, esplanade de la Défense","Tour Franklin","92060","LA DEFENSE");
    Particuliers *p5 = new Particuliers(9,"BENSAID","3, avenue des Parcs","","93500","ROISSY EN FRANCE","bensaid@gmail.com","Georgia","Féminin",QDate(1976,04,16));
    Professionnel *pro5 = new Professionnel(10,"LEONIDAS","15, Place de la Bastille","Fond de Cour","75003","PARIS","contact@leonidas.fr", "91235987456832","SAS","10, rue de la Paix","","75008","PARIS");
    Particuliers *p6 = new Particuliers(11,"ABABOU","3, rue Lecourbe","","93200","BAGNOLET","ababou@gmail.com","Teddy","Masculin",QDate(1970,10,10));

    mapClients.insert(p1->getIdentifiant(), p1);
    mapClients.insert(pro1->getIdentifiant(),pro1);
    mapClients.insert(p2->getIdentifiant(),p2);
    mapClients.insert(pro2->getIdentifiant(),pro2);
    mapClients.insert(p3->getIdentifiant(),p3);
    mapClients.insert(pro3->getIdentifiant(),pro3);
    mapClients.insert(p4->getIdentifiant(),p4);
    mapClients.insert(pro4->getIdentifiant(),pro4);
    mapClients.insert(p5->getIdentifiant(),p5);
    mapClients.insert(pro5->getIdentifiant(),pro5);
    mapClients.insert(p6->getIdentifiant(),p6);

    for(auto cli: mapClients.values())
    {
        qDebug() << cli->getNom();
    }
}

QMap<int, Client *> InitMap::getMapClients()
{
    return mapClients;
}
