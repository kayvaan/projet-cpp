#include "mainwindow.h"
#include <QApplication>

#include "adresse.h"
#include "client.h"
#include "professionnel.h"
#include "particuliers.h"
#include "initmap.h"

int main(int argc, char *argv[])
{
    InitMap::alimenter();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

/* Test
    Client c1(1,"BETY","12,rue des Oliviers","",94000,"CRETEIL","bety@gmail.com");
    c1.afficherClient();

    Professionnel pro1(2,"AXA","125,rue Lafayette","Digicode 1432",94120,"FONTENAY SOUS BOIS","info@axa.fr",12548795641122,"SARL","125,rue Lafayette","Digicode 1432",94120,"FONTENAY SOUS BOIS");
    pro1.afficherPro();

    Particuliers p1(3,"BODIN","10,rue des Oliviers","Etage 2",94300,"VINCENNES","bodin@gmail.com","Justin","Masculin",QDate(1965,5,5));
    p1.afficherParticulier();
*/

    return a.exec();
}
