#ifndef INITMAP_H
#define INITMAP_H

#include "client.h"

#include <QMap>



class InitMap
{
public:

    static void alimenter();
    static QMap<int, Client *> getMapClients();
    static QMap<int, Client*> mapClients;
};

#endif // INITMAP_H
