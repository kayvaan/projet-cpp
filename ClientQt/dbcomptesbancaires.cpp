#include "dbcomptesbancaires.h"
#include "ui_dbcomptesbancaires.h"

#include <QCloseEvent>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlTableModel>
#include <QTableWidget>
#include <QTableView>
#include <QDir>

DbComptesBancaires::DbComptesBancaires(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DbComptesBancaires)
{
    ui->setupUi(this);
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("C:\\Users\\Axe\\Desktop\\Formation AJC Dev C++\\Langage C++\\Projet C++\\BdComptes.db");
    if(QFile("C:\\Users\\Axe\\Desktop\\Formation AJC Dev C++\\Langage C++\\Projet C++\\BdComptes.db").exists())
        {
            db.setDatabaseName("C:\\Users\\Axe\\Desktop\\Formation AJC Dev C++\\Langage C++\\Projet C++\\BdComptes.db");
            bool ok = db.open();
            if(ok)
            {
                qDebug() << "Connexion établie !!!";
                //db.close();
            }
            else
            {
                qDebug() << "Echec connexion !!!";
            }
        }
        else
        {
            qDebug() << "Base de données BdComptes.db introuvable !!!";
        }
    QSqlQueryModel *model = new QSqlQueryModel(this);
    model->setQuery("SELECT numcompte, datecreation, solde, decouvert, numcli FROM comptes ORDER BY numcompte");
    ui->tableDb->setModel(model);

    ui->tableDb->setSelectionMode(QAbstractItemView::SingleSelection); // 1 Seule ligne selectionnable
    ui->tableDb->setSelectionBehavior(QAbstractItemView::SelectRows); // Toute la ligne est selectionné auto!!!

    db.close();

}

DbComptesBancaires::~DbComptesBancaires()
{
    delete ui;
}

void DbComptesBancaires::on_btnQuitter_clicked()
{
    this->close();
}

void DbComptesBancaires::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox(this);
    msgBox.setText("Fermeture Imminente de la Fenêtre");
    msgBox.setInformativeText(QString("Êtes-vous sûr de vouloir quitter ?"));
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret=msgBox.exec();
    if(ret==QMessageBox::Yes)
    {
        this->close();
    }
    else
    {
        event->ignore();
    }
}
