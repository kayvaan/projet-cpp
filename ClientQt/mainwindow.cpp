#include "mainwindow.h"
#include "particuliers.h"
#include "professionnel.h"
#include "initmap.h"
#include "listingclients.h"
#include "ui_mainwindow.h"
#include "dbcomptesbancaires.h"

#include <QCloseEvent>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlTableModel>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_Quitter_triggered()
{
    QMessageBox msgBox(this);
    msgBox.setText("Fermeture Imminente");
    msgBox.setInformativeText(QString("Êtes-vous sûr de vouloir quitter ?"));
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret=msgBox.exec();
    if(ret==QMessageBox::Yes)
    {
        this->close();
    }
}

void MainWindow::on_btnListClients_clicked()
{
    ListingClients *fenetreListing = new ListingClients();
    fenetreListing->show();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox(this);
    msgBox.setText("Fermeture Imminente de la Fenêtre");
    msgBox.setInformativeText(QString("Êtes-vous sûr de vouloir quitter ?"));
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret=msgBox.exec();
    if(ret==QMessageBox::Yes)
    {
        this->close();
    }
    else
    {
        event->ignore();
    }
}

void MainWindow::on_btnQuitter_clicked()
{
    this->close();
}

void MainWindow::on_btnConsultComptes_clicked()
{
    DbComptesBancaires *fenetreDb = new DbComptesBancaires();
    fenetreDb->show();
}
