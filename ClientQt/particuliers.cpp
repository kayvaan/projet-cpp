#include "particuliers.h"

Particuliers::Particuliers(int ID, QString n, QString lib, QString comp, QString CP, QString ville, QString mail, QString p,QString s,QDate birthday)
    :Client(ID,n,lib,comp,CP,ville,mail)
{
    this->setPrenom(p);
    this->setSexe(s);
    this->setDateNaissance(birthday);
}

Particuliers::~Particuliers()
{
    qDebug() << "Destruction du client Particulier : "
             << getPrenom() << getNom();
}

QDate Particuliers::getDateNaissance() const
{
    return dateNaissance;
}

void Particuliers::setDateNaissance(const QDate &value)
{
    dateNaissance = value;
}

QString Particuliers::getPrenom() const
{
    return prenom;
}

void Particuliers::setPrenom(const QString &value)
{
    prenom = value;
}

QString Particuliers::getSexe() const
{
    return sexe;
}

void Particuliers::setSexe(const QString &value)
{
    sexe = value;
}

void Particuliers::afficherParticulier()
{
    QString details = "Client ID : " + (QString::number(getIdentifiant()))
            + ", nom : " + getNom() + " habitant à l'adresse : " + adr->getLibelle() + " "
            + adr->getComplement() + " " + adr->getVille() + " "
            + adr->getCodePostale() +
            ", adresse mail associee : " + getMail() + ", prénomé "
            + getPrenom() + ", de sexe " + getSexe()
            + ", né le " + getDateNaissance().toString("dd/MM/yyyy");
    qDebug() << details;
}


