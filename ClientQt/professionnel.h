#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H

#include "client.h"

class Professionnel : public Client
{
public:
    Professionnel(int,QString,QString,QString,QString,QString,QString,QString,QString,QString,QString,QString,QString);

    virtual ~Professionnel();

    virtual QString getSIRET() const;
    void setSIRET(QString value);

    virtual QString getStatutJuridique() const;
    void setStatutJuridique(const QString &value);

    void afficherPro();

    Adresse *adrPro=nullptr;
    Adresse *getAdrPro() const;

private:
    QString SIRET;
    QString statutJuridique;

};

#endif // PROFESSIONNEL_H
