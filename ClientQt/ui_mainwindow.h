/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Quitter;
    QWidget *centralWidget;
    QLabel *lblBienvenue;
    QPushButton *btnListClients;
    QPushButton *btnConsultComptes;
    QPushButton *btnImporOpeBancaires;
    QPushButton *btnQuitter;
    QMenuBar *menuBar;
    QMenu *menuFichier;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(742, 645);
        action_Quitter = new QAction(MainWindow);
        action_Quitter->setObjectName(QString::fromUtf8("action_Quitter"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        lblBienvenue = new QLabel(centralWidget);
        lblBienvenue->setObjectName(QString::fromUtf8("lblBienvenue"));
        lblBienvenue->setGeometry(QRect(70, 40, 611, 51));
        QFont font;
        font.setFamily(QString::fromUtf8("Verdana"));
        font.setPointSize(16);
        lblBienvenue->setFont(font);
        lblBienvenue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        btnListClients = new QPushButton(centralWidget);
        btnListClients->setObjectName(QString::fromUtf8("btnListClients"));
        btnListClients->setGeometry(QRect(200, 130, 361, 41));
        btnConsultComptes = new QPushButton(centralWidget);
        btnConsultComptes->setObjectName(QString::fromUtf8("btnConsultComptes"));
        btnConsultComptes->setGeometry(QRect(200, 200, 361, 41));
        btnImporOpeBancaires = new QPushButton(centralWidget);
        btnImporOpeBancaires->setObjectName(QString::fromUtf8("btnImporOpeBancaires"));
        btnImporOpeBancaires->setGeometry(QRect(200, 270, 361, 41));
        btnQuitter = new QPushButton(centralWidget);
        btnQuitter->setObjectName(QString::fromUtf8("btnQuitter"));
        btnQuitter->setGeometry(QRect(620, 500, 80, 21));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 742, 20));
        menuFichier = new QMenu(menuBar);
        menuFichier->setObjectName(QString::fromUtf8("menuFichier"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFichier->menuAction());
        menuFichier->addAction(action_Quitter);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        action_Quitter->setText(QCoreApplication::translate("MainWindow", "&Quitter", nullptr));
        lblBienvenue->setText(QCoreApplication::translate("MainWindow", "Bienvenue dans l'interface de Gestion de comptes clients", nullptr));
        btnListClients->setText(QCoreApplication::translate("MainWindow", "Listing des Clients", nullptr));
        btnConsultComptes->setText(QCoreApplication::translate("MainWindow", "Consultation des Comptes", nullptr));
        btnImporOpeBancaires->setText(QCoreApplication::translate("MainWindow", "Importation des Op\303\251rations Bancaires", nullptr));
        btnQuitter->setText(QCoreApplication::translate("MainWindow", "Quitter", nullptr));
        menuFichier->setTitle(QCoreApplication::translate("MainWindow", "&Fichier", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
