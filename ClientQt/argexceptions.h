#ifndef ARGEXCEPTIONS_H
#define ARGEXCEPTIONS_H

#include <QObject>
#include <QException>
#include <exception>
#include <sstream>
#include <QDebug>
#include <QTextStream>

#include "enumerationerr.h"

using namespace  std;

class ArgExceptions
{

private:
    Erreurs codeErr;
    mutable QString message;

public:
    ArgExceptions(Erreurs);
    virtual ~ArgExceptions();

    enum Erreurs GetcodeErr() const { return codeErr; }

    QString Getmessage() const;

    virtual const char* what();

    //explicit ArgExceptions(QObject *parent = nullptr);

};

#endif // ARGEXCEPTIONS_H
