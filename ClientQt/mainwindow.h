#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QtDebug>
#include <QFile>
#include <QString>
#include <QSqlQuery>
#include <QMessageBox>
#include <QListWidget>


namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_action_Quitter_triggered();

    void closeEvent(QCloseEvent *event);

    void on_btnListClients_clicked();

    void on_btnQuitter_clicked();

    void on_btnConsultComptes_clicked();

private:
    Ui::MainWindow *ui;
    QSqlDatabase db;
};

#endif // MAINWINDOW_H
