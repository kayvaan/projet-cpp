#include "listingclients.h"
#include "ui_listingclients.h"

#include <QMessageBox>
#include <QAbstractTableModel>
#include <QTableView>
#include <QListWidget>
#include <QDebug>
#include <QCloseEvent>
#include "initmap.h"
#include "client.h"
#include "professionnel.h"
#include "particuliers.h"
#include "adresse.h"

ListingClients::ListingClients(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ListingClients)
{
    ui->setupUi(this);

    for(auto cli: InitMap::mapClients.values())
    {
        ui->lstClients->addItem(cli->GetIdentQStr() + " " + cli->getNom());
    }
}

ListingClients::~ListingClients()
{
    delete ui;
}

void ListingClients::on_btnQuitter_clicked()
{
    this->close();
}



void ListingClients::on_lstClients_itemDoubleClicked(QListWidgetItem *item)
{
    QString cle = item->text();
    cle.truncate(5);
    int num = cle.toInt();
    QMap<int, Client*> mesClients = InitMap::getMapClients();
    Client* cli = mesClients[num];
    if(cli != nullptr)
    {
        ui->lstDetails->clear();

        Particuliers* part = dynamic_cast<Particuliers*>(cli);

        if(part!=nullptr)
        {
            QString prefix = "Mme";// pour afficher le prefix en fonction du sexe.
            if(part->getSexe() == "Masculin")
            {
                prefix = "Mr";
            }
            QDate today = today.currentDate();
            int age = today.year() - part->getDateNaissance().year();
            int testm = today.month() - part->getDateNaissance().month();
            int testj = today.day() - part->getDateNaissance().day();
            QString infoage = QString::number(age) + " ans";
            if(testm == 0 && testj == 0)
            {
                QMessageBox msgBox(this);
                msgBox.setText(infoage + " et Bon anniversaire !!!");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setStandardButtons(QMessageBox::Yes);
                msgBox.setDefaultButton(QMessageBox::Yes);
                int ret=msgBox.exec();
                if(ret==QMessageBox::Yes)
                {
                    msgBox.close();
                }
            }
            ui->lstDetails->addItem(prefix +" "+cli->getNom()+" "+ part->getPrenom());
            ui->lstDetails->addItem("Adresse :" + part->adr->getLibelle() + " " + part->adr->getComplement() + " "
                                    + cli->adr->getVille() + " "
                                    + part->adr->getCodePostale());
            ui->lstDetails->addItem("Né le " + part->getDateNaissance().toString() + ", et a " + infoage);
        }
        Professionnel* pro = dynamic_cast<Professionnel*>(cli);
        if(pro!=nullptr)
        {
            ui->lstDetails->addItem(cli->getNom());
            ui->lstDetails->addItem("Adresse :" +pro->adr->getLibelle() + " "
                                    + pro->adr->getComplement() + " " + pro->adr->getCodePostale()
                                    + " " + pro->adr->getVille());
            ui->lstDetails->addItem("Adresse mail : " + pro->getMail());
            ui->lstDetails->addItem("N° SIRET : " + pro->getSIRET());
            ui->lstDetails->addItem("Statut Juridique : " + pro->getStatutJuridique());
            ui->lstDetails->addItem("Adresse Siège : " + pro->adrPro->getLibelle() + " "
                                    + pro->adrPro->getComplement() + " " + pro->adrPro->getCodePostale() +" "
                                    + pro->adrPro->getVille());
        }
    }
    else
    {
        qDebug() << "Client inexistant";
    }
}


void ListingClients::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox(this);
    msgBox.setText("Fermeture Imminente de la Fenêtre");
    msgBox.setInformativeText(QString("Êtes-vous sûr de vouloir quitter ?"));
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret=msgBox.exec();
    if(ret==QMessageBox::Yes)
    {
        this->close();
    }
    else
    {
        event->ignore();
    }
}
