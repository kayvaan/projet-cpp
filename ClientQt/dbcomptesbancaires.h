#ifndef DBCOMPTESBANCAIRES_H
#define DBCOMPTESBANCAIRES_H

#include <QDialog>

namespace Ui {
class DbComptesBancaires;
}

class DbComptesBancaires : public QDialog
{
    Q_OBJECT

public:
    explicit DbComptesBancaires(QWidget *parent = nullptr);
    ~DbComptesBancaires();

private slots:

    void on_btnQuitter_clicked();

    void closeEvent(QCloseEvent *event);

private:
    Ui::DbComptesBancaires *ui;
};

#endif // DBCOMPTESBANCAIRES_H
