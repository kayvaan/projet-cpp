#ifndef PARTICULIERS_H
#define PARTICULIERS_H

#include <QDate>
#include "client.h"


class Particuliers
        : public Client
{
public:
    Particuliers(int,QString,QString,QString,QString,QString,QString,QString,QString,QDate);

    virtual ~Particuliers();

    QDate getDateNaissance() const;
    void setDateNaissance(const QDate &value);

    QString getPrenom() const;
    void setPrenom(const QString &value);


    QString getSexe() const;
    void setSexe(const QString &value);

    void afficherParticulier();

private:
    QString prenom;
    QString sexe;
    QDate dateNaissance;
};

#endif // PARTICULIERS_H
