/********************************************************************************
** Form generated from reading UI file 'dbcomptesbancaires.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DBCOMPTESBANCAIRES_H
#define UI_DBCOMPTESBANCAIRES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>

QT_BEGIN_NAMESPACE

class Ui_DbComptesBancaires
{
public:
    QPushButton *btnQuitter;
    QTableView *tableDb;

    void setupUi(QDialog *DbComptesBancaires)
    {
        if (DbComptesBancaires->objectName().isEmpty())
            DbComptesBancaires->setObjectName(QString::fromUtf8("DbComptesBancaires"));
        DbComptesBancaires->resize(758, 721);
        btnQuitter = new QPushButton(DbComptesBancaires);
        btnQuitter->setObjectName(QString::fromUtf8("btnQuitter"));
        btnQuitter->setGeometry(QRect(650, 670, 80, 21));
        tableDb = new QTableView(DbComptesBancaires);
        tableDb->setObjectName(QString::fromUtf8("tableDb"));
        tableDb->setGeometry(QRect(120, 80, 561, 561));

        retranslateUi(DbComptesBancaires);

        QMetaObject::connectSlotsByName(DbComptesBancaires);
    } // setupUi

    void retranslateUi(QDialog *DbComptesBancaires)
    {
        DbComptesBancaires->setWindowTitle(QCoreApplication::translate("DbComptesBancaires", "Dialog", nullptr));
        btnQuitter->setText(QCoreApplication::translate("DbComptesBancaires", "Quitter", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DbComptesBancaires: public Ui_DbComptesBancaires {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DBCOMPTESBANCAIRES_H
