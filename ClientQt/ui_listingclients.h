/********************************************************************************
** Form generated from reading UI file 'listingclients.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LISTINGCLIENTS_H
#define UI_LISTINGCLIENTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_ListingClients
{
public:
    QPushButton *btnQuitter;
    QListWidget *lstClients;
    QListWidget *lstDetails;

    void setupUi(QDialog *ListingClients)
    {
        if (ListingClients->objectName().isEmpty())
            ListingClients->setObjectName(QString::fromUtf8("ListingClients"));
        ListingClients->resize(600, 475);
        btnQuitter = new QPushButton(ListingClients);
        btnQuitter->setObjectName(QString::fromUtf8("btnQuitter"));
        btnQuitter->setGeometry(QRect(480, 420, 80, 21));
        lstClients = new QListWidget(ListingClients);
        lstClients->setObjectName(QString::fromUtf8("lstClients"));
        lstClients->setGeometry(QRect(30, 40, 141, 91));
        lstDetails = new QListWidget(ListingClients);
        lstDetails->setObjectName(QString::fromUtf8("lstDetails"));
        lstDetails->setGeometry(QRect(30, 150, 551, 251));

        retranslateUi(ListingClients);

        QMetaObject::connectSlotsByName(ListingClients);
    } // setupUi

    void retranslateUi(QDialog *ListingClients)
    {
        ListingClients->setWindowTitle(QCoreApplication::translate("ListingClients", "Dialog", nullptr));
        btnQuitter->setText(QCoreApplication::translate("ListingClients", "Quitter", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ListingClients: public Ui_ListingClients {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LISTINGCLIENTS_H
