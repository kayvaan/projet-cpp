#include "adresse.h"


Adresse::Adresse(QString lib, QString comp, QString CP, QString ville)
{
    this->setLibelle(lib);
    this->setComplement(comp);
    this->setCodePostale(CP);
    this->setVille(ville);
}

Adresse::~Adresse()
{
    qDebug() << "Destruction de Adresse ";
}

QString Adresse::getLibelle() const
{
    return libelle;
}

void Adresse::setLibelle(const QString &value)
{
    libelle = value;
}

QString Adresse::getComplement() const
{
    return complement;
}

void Adresse::setComplement(const QString &value)
{
    complement = value;
}

QString Adresse::getCodePostale() const
{
    return CodePostale;
}

void Adresse::setCodePostale(QString value)
{
    CodePostale = value;
}

QString Adresse::getVille() const
{
    return ville;
}

void Adresse::setVille(const QString &value)
{
    ville = value;
}
