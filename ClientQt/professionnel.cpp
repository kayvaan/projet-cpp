#include "professionnel.h"

Professionnel::Professionnel(int ID, QString n, QString lib, QString comp, QString CP, QString ville, QString mail, QString Siret, QString Statut, QString libPro, QString compPro, QString CPpro, QString villePro)
    :Client(ID,n,lib,comp,CP,ville,mail)
{
    this->setSIRET(Siret);
    this->setStatutJuridique(Statut);
    this->adrPro = new Adresse(libPro,compPro,CPpro,villePro);
}

Professionnel::~Professionnel()
{
    qDebug() << "Destruction de Professionnel " << getNom() << getSIRET();
}

QString Professionnel::getSIRET() const
{
    return SIRET;
}

void Professionnel::setSIRET(QString value)
{
    SIRET = value;
}

QString Professionnel::getStatutJuridique() const
{
    return statutJuridique;
}

void Professionnel::setStatutJuridique(const QString &value)
{
    statutJuridique = value;
}

void Professionnel::afficherPro()
{
    QString details = "Client ID : " + QString::number(getIdentifiant())
            + ", nom : " + getNom() + " habitant à l'adresse : " + adr->getLibelle() + " "
            + adr->getComplement() + " " + adr->getCodePostale() + " " + adr->getVille() +
            " Adresse mail associee : " + getMail() + "."
            + " Numéro SIRET : " + getSIRET() + "."
            + " Statut juridique : " + getStatutJuridique() + "."
            + " Siège : " + adrPro->getLibelle() + " " + adrPro->getComplement() + " "
            + adrPro->getCodePostale() + " " + adrPro->getVille();
    qDebug() << details;
}

Adresse *Professionnel::getAdrPro() const
{
    return adrPro;
}
